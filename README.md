# Marvel characters

## Project description :star:

 - The user must enter the access data and when clicking on the “access” button.
 - On the main screen, there is a list with the name of each character.
 - Clicking on a character will show data about it.


## How to run the application :arrow_forward:

In case of download the project you will need:

**Recommended:** Install or have installed Node.js - version **14.16.0** or higher.

**Recommended:** Install or have Yarn installed.

- Run the command below to download all dependencies:

```
yarn
```
- To start the project, just execute the command:

```
yarn dev
```
## Technologies used 🚀

 - [*React*](https://reactjs.org/)
 - [*Redux-Saga*](https://redux-saga.js.org/)
 - [*BabelJS*](https://babeljs.io/)
 - [*Webpack.js*](https://webpack.js.org/)
 - [*Styled Components*](https://styled-components.com/)
 - [*Eslint*](https://eslint.org/)
 - [*Prettier*](https://prettier.io/)

