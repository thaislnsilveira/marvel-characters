import React from 'react'
import { Switch } from 'react-router-dom'
import Route from './Route'

import SignIn from '../pages/SignIn'
import Characters from '../pages/Characters'
import Character from '../pages/Character'

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />
      <Route path="/characters" component={Characters} isPrivate />
      <Route path="/character/:id" component={Character} isPrivate />
    </Switch>
  )
}
