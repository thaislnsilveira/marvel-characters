import styled from 'styled-components'

export const Container = styled.div`
  background: #f5f5f5;
  div {
    max-width: 1300px;
    margin: 0 auto;
    padding-top: 40px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    span {
      flex-shrink: 0;
      flex-grow: 1 !important;
      font-size: 24px;
      font-weight: bold;
      padding-left: 30px;
      margin-right: auto;
    }
  }
`
