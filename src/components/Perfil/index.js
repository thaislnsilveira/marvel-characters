import React from 'react'

import { Container } from './styles'

export default function Perfil({ children }) {
  return <Container>{children}</Container>
}
