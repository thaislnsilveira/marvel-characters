import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 100%;

  img {
    height: 200px;
    width: 200px;
    border-radius: 0 10px;
    margin: 0 15px;
  }
  .perfil-information {
    padding: 20px;
    color: #202020;
    letter-spacing: 0.5px;
    line-height: 1.2;
    h1 {
      margin-bottom: 20px;
      font-weight: 700;
      font-size: 38px;
      line-height: 1.1;
    }
    p {
      font-size: 15px;
    }
  }
  .perfil-button {
    button {
      padding: 10px 20px;
      color: #fff;
      background-color: #e62429;
      font-weight: 700;
      outline: none;
      border: none;
    }
  }
`
