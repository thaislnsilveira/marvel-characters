import React from 'react'
import { format, parseISO } from 'date-fns'
import { ptBR } from 'date-fns/locale'

import { useSelector, useDispatch } from 'react-redux'

import ActionContent from '~/components/ActionContent'
import ActionHeader from '~/components/ActionHeader'
import DefaultTable from '~/components/DefaultTable'

import { getCharacterByIdRequest } from '~/store/modules/character/actions'

export default function Characters() {
  const heros = useSelector((state) => state.auth.characters.data.results)
  const hash = useSelector((state) => state.auth.hash)
  const public_key = useSelector((state) => state.auth.public_key)

  const dispatch = useDispatch()

  function handleCharacter(id) {
    dispatch(getCharacterByIdRequest(id, hash, public_key))
  }

  return (
    <>
      <ActionHeader>
        <div>
          <span>Character List</span>
        </div>
      </ActionHeader>
      <ActionContent>
        <DefaultTable>
          <thead>
            <tr>
              <th>Nome</th>
              <th>Descrição</th>
              <th>Última atualização</th>
            </tr>
          </thead>
          <tbody>
            {heros &&
              heros.map((hero) => (
                <tr key={hero.id} onClick={() => handleCharacter(hero.id)}>
                  <td>{hero.name}</td>
                  <td>{hero.description}</td>
                  <td>
                    {format(parseISO(hero.modified), 'dd MMM yyyy', {
                      locale: ptBR
                    })}
                  </td>
                </tr>
              ))}
          </tbody>
        </DefaultTable>
      </ActionContent>
    </>
  )
}
