import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Input } from '@rocketseat/unform'
import * as Yup from 'yup'

import { signInRequest } from '~/store/modules/auth/actions'

const schema = Yup.object().shape({
  private_key: Yup.string().required('Private key é obrigatório.'),
  public_key: Yup.string().required('Public key é obrigatório.')
})

export default function SignIn() {
  const dispatch = useDispatch()
  const loading = useSelector((state) => state.auth.loading)
  function handleSubmit({ private_key, public_key }) {
    dispatch(signInRequest(private_key, public_key))
  }
  return (
    <>
      <div className="panel">
        <Form schema={schema} onSubmit={handleSubmit}>
          <h1>Dados de acesso</h1>
          <Input name="private_key" type="text" placeholder="private_key" />
          <Input name="public_key" type="text" placeholder="public_key" />

          <button type="submit">{loading ? 'Carregando...' : 'Acessar'}</button>
        </Form>
      </div>
    </>
  )
}
