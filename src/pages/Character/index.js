import React from 'react'

import { useSelector } from 'react-redux'

import ActionHeader from '~/components/ActionHeader'
import ActionContent from '~/components/ActionContent'
import Perfil from '~/components/Perfil'

import history from '../../services/history'

export default function Character() {
  const hero = useSelector((state) => state.character.character.data.results)

  return (
    <>
      <ActionHeader>
        <div>
          <span>Character</span>
        </div>
      </ActionHeader>
      <ActionContent>
        <Perfil>
          <img
            src={`${hero[0].thumbnail.path}.${hero[0].thumbnail.extension}`}
            alt={hero[0].name}
          />
          <div className="perfil-information">
            <h1>{hero[0].name}</h1>
            <p>{hero[0].description}</p>
          </div>
          <div className="perfil-button">
            <button onClick={() => history.push('/')}>Voltar</button>
          </div>
        </Perfil>
      </ActionContent>
    </>
  )
}
