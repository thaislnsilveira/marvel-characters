import produce from 'immer'

const INITIAL_STATE = {
  character: null,
  loading: false
}

export default function character(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@character/GET_CHARACTER_BY_ID_REQUEST': {
        draft.loading = true
        break
      }
      case '@character/GET_CHARACTER_BY_ID': {
        draft.character = action.payload.character
        draft.loading = false
        break
      }
      case '@character/SIGN_FAILURE': {
        draft.loading = false
        break
      }
      default:
    }
  })
}
