import { takeLatest, call, put, all } from 'redux-saga/effects'
import { toast } from 'react-toastify'

import history from '../../../services/history'
import api from '../../../services/api'

import { getCharacterById, signFailure } from './actions'

export function* getCharacter({ payload }) {
  try {
    const { id, hash, public_key } = payload

    const response = yield call(api.get, `characters/${id}`, {
      params: {
        apikey: public_key,
        hash: hash
      }
    })

    const data = response.data

    yield put(getCharacterById(data))

    history.push(`character/${id}`)
  } catch (err) {
    toast.error('failed to fetch data')
    yield put(signFailure())
  }
}

export default all([
  takeLatest('@character/GET_CHARACTER_BY_ID_REQUEST', getCharacter)
])
