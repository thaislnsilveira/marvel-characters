export function getCharacterByIdRequest(id, hash, public_key) {
  return {
    type: '@character/GET_CHARACTER_BY_ID_REQUEST',
    payload: { id, hash, public_key }
  }
}

export function getCharacterById(character) {
  return {
    type: '@character/GET_CHARACTER_BY_ID',
    payload: { character }
  }
}

export function signFailure() {
  return {
    type: '@character/SIGN_FAILURE'
  }
}
