export function signInRequest(private_key, public_key) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { private_key, public_key }
  }
}

export function signInSuccess(characters, hash, public_key) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { characters, hash, public_key }
  }
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE'
  }
}

export function signOut() {
  return {
    type: '@auth/SIGN_OUT'
  }
}
