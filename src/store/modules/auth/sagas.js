import { takeLatest, call, put, all } from 'redux-saga/effects'
import { toast } from 'react-toastify'
import md5 from 'crypto-js/md5'

import history from '../../../services/history'
import api from '../../../services/api'

import { signInSuccess, signFailure } from './actions'

export function* signIn({ payload }) {
  try {
    const { private_key, public_key } = payload

    const timestamp = Math.floor(Date.now() / 1000)

    const hash = md5(`${timestamp}${public_key}${private_key}`).toString()

    const response = yield call(api.get, 'characters', {
      params: {
        apikey: public_key,
        hash: hash
      }
    })

    const data = response.data

    yield put(signInSuccess(data, hash, public_key))

    toast.success('Welcome!')

    history.push('/characters')
  } catch (err) {
    toast.error('Authentication failed, check your data')
    yield put(signFailure())
  }
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)])
