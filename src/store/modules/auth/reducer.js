import produce from 'immer'

const INITIAL_STATE = {
  public_key: null,
  hash: null,
  characters: null,
  signed: false,
  loading: false
}

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true
        break
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.characters = action.payload.characters
        draft.hash = action.payload.hash
        draft.public_key = action.payload.public_key
        draft.signed = true
        draft.loading = false
        break
      }
      case '@auth/SIGN_FAILURE': {
        draft.loading = false
        break
      }
      default:
    }
  })
}
